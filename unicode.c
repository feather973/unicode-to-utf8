#include <stdio.h>
#include <locale.h>
#include <wchar.h>

// https://ko.wikipedia.org/wiki/UTF-8

// unicode 2 bytes --> utf-8 1/2/3 bytes
char* encode_utf8(wchar_t unicode) {
	char* res = (char*)malloc(3);
	memset(res, 0x00, 3);

	char tmp1, tmp2, tmp3;

	if (unicode < 0x80) {
		// unicode : 00000000 0xxxxxxx
		// utf-8   :          0xxxxxxx

		res[0] = unicode & 0x7f;
	}
	else if (unicode < 0x800) {
		// unicode : 00000xxx xxxxxxxx
		// utf-8   : 110xxxxx 10xxxxxx

		tmp1 = (unicode & 0x0700) >> 8;
		tmp2 = unicode & 0x00ff;

		res[0] = 0xc0;
		res[0] |= tmp1;

		res[1] = 0x80;
		res[1] |= tmp2;

	}
	else if (unicode < 0x10000) {
		// unicode : xxxxxxxx xxxxxxxx
		// utf-8   : 1110xxxx 10xxxxxx 10xxxxxx

		tmp1 = (unicode & 0xf000) >> 12;
		tmp2 = (unicode & 0x0fc0) >> 6;
		tmp3 = unicode & 0x003f;

		res[0] = 0xe0;
		res[0] |= tmp1;

		res[1] = 0x80;
		res[1] |= tmp2;

		res[2] = 0x80;
		res[2] |= tmp3;

	}
	else if (unicode < 0x110000) {
		// unicode : 110110ZZ ZZxxxxxx 110111xx xxxxxxxx
		// utf-8   : 11110zzz 10zzxxxx 10xxxxxx 10xxxxxx

		printf("UTF-16 Surrogate not supported!\n");
		return 0;
	}

	//	printf("%x %x %x\n", res[0], res[1], res[2]);	 // DEBUG
	return res;
}

// utf-8 3 bytes --> unicode 2 bytes
void decode_utf8(unsigned char* res, char utf8_1, char utf8_2, char utf8_3) {
	if (utf8_2 == 0x00) {
		// utf-8   :          0xxxxxxx
		// unicode : 0xxxxxxx 00000000

		res[0] = utf8_1 & 0x7f;
		res[1] = 0x00;
	}
	else if (utf8_3 == 0x00) {
		// utf-8   : 110xxxxx 10xxxxxx
		// unicode : 00000xxx xxxxxxxx

		res[0] = (utf8_1 & 0x1c) >> 2;
		res[1] = (utf8_1 & 0x03) << 6;
		res[1] |= utf8_2 & 0x3f;
	}
	else {
		// utf-8   : 1110xxxx 10xxxxxx 10xxxxxx
		// unicode : xxxxxxxx xxxxxxxx

		res[0] = (utf8_1 & 0x0f) << 4;
		res[0] |= (utf8_2 & 0x3c) >> 2;

		res[1] = (utf8_2 & 0x03) << 6;
		res[1] |= utf8_3 & 0x3f;

	}

	//printf("%x %x\n", res[0], res[1]);				// DEBUG
	return;
}


int main(int argc, char** argv) {
	char* ptr;
	wchar_t table[24] = {
		L'A', L'\0', L'B', L'\0', L'C', L'\0', L'D', L'\0', L'E', L'\0',
		L'0', L'\0', L'1', L'\0', L'2', L'\0', L'3', L'\0', L'이', L'\0',
		L'원', L'\0', L'국', L'\0'
	};

	// set locale
	setlocale(LC_ALL, "");

	// check character map
	printf("%s\n", setlocale(LC_CTYPE, NULL));

	// check whether utf-8 or euc-kr encoding ( korean encoding )
//	if (strstr(setlocale(LC_CTYPE, NULL), "949") != NULL) {
//		printf("EUC-KR not supported, please use UTF-8 encoding\n");
//		printf("Unicode use UTF-8 encoding\n");
//
//		return -1;
//	}

	printf("==== CHAR -- UNICODE -- UTF8 TABLE ====\n");

	// print table
	for (int i = 0; i < 24; i++) {
		if (i % 2 == 1)		continue;

		ptr = encode_utf8(table[i]);
		fprintf(stdout, "%ls -- U+%X -- %X %X %X\n",
			table + i, *(table + i), ptr[0] & 0xff, ptr[1] & 0xff, ptr[2] & 0xff);
		free(ptr);
	}
	printf("\n");

	// print argv
	// argv alreay utf-8 ( unicode's encoding scheme )
	// so, first decode utf-8 to get unicode, and encode it to utf-8
	if (argc < 2) {
		printf("no argument\n");
	}
	else {
		unsigned char hex[2];			// unsigned char : whole 8 bit is needed
										// for utf-8 decode
		wchar_t unicode[2];

		printf("==== CHAR -- UNICODE -- UTF8 CONVERSION ====\n");
		for (int i = 1; i < argc; i++) {
			// decode utf-8 to unicode
			decode_utf8(hex, *(argv[i]), *(argv[i] + 1), *(argv[i] + 2));
			if(hex[0] > 0x7f)
				unicode[0] = hex[0] << 8 | hex[1];
			else
				unicode[0] = hex[1] << 8 | hex[0];
		
			unicode[1] = L'\0';
			// encode unicode to utf-8
			ptr = encode_utf8(unicode[0]);
			fprintf(stdout, "%ls -- U+%X -- %X %X %X\n",
				unicode, *unicode, ptr[0] & 0xff, ptr[1] & 0xff, ptr[2] & 0xff);
			free(ptr);
		}
	}

	return 0;
}

